This site is only for issues.

Code is on <a href="http://github.com/gnachman/iTerm2">Github</a>.

The main project page is at <a href="https://iterm2.com">iterm2.com</a>.

Want to report a bug? Please include the following information:
  * A copy of ~/Library/Preferences/com.googlecode.iterm2.plist
  * The version of iTerm2 you are running
  * OS version
  * A <a href="https://code.google.com/p/iterm2/wiki/DebugLogging">debug log</a>
  * A <a href="https://code.google.com/p/iterm2/wiki/HowToSample">sample</a> of the process, if this is a performance issue.

